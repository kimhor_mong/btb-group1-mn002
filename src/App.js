import { useState } from 'react';
import PersonInfoCreate from './components/PersonInfoCreate';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'font-awesome/css/font-awesome.css';
import NavMenu from './components/NavMenu';
import TableData from './components/TableData';
import Page from './components/Page';
function App() {
	const [personInfo, setPersonInfo] = useState([
		{
			id: 1,
			username: 'Mark',
			gender: 'Male',
			email: 'mark@gmail.com',
			jobs: [
				{ id: 1, value: 'Student', isChecked: false },
				{ id: 2, value: 'Teacher', isChecked: true },
				{ id: 3, value: 'Developer', isChecked: false },
			],
			createdAt: 'one minute ago',
			updatedAt: 'two minute ago',
		},
		{
			id: 2,
			username: 'jacob',
			gender: 'Male',
			email: 'jacob@gmail.com',
			jobs: [
				{ id: 1, value: 'Student', isChecked: true },
				{ id: 2, value: 'Teacher', isChecked: false },
				{ id: 3, value: 'Developer', isChecked: true },
			],
			createdAt: 'one minute ago',
			updatedAt: 'two minute ago',
		},
		{
			id: 3,
			username: 'Marry',
			gender: 'Female',
			email: 'Mary@gmail.com',
			jobs: [
				{ id: 1, value: 'Student', isChecked: true },
				{ id: 2, value: 'Teacher', isChecked: false },
				{ id: 3, value: 'Developer', isChecked: false },
			],
			createdAt: 'one minute ago',
			updatedAt: 'two minute ago',
		},
	]);

	const [selectedPersonInfo, setSeletedPersonInfo] = useState({});

	const deletePersonInfo = (id) => {
		let newPersonInfo = [...personInfo];
		newPersonInfo = newPersonInfo.filter((info) => info.id !== id);
		setPersonInfo(newPersonInfo);
	};

	const addPersonInfo = (newPersonInfo) => {
		let newPersonInfos = [...personInfo];

		if (newPersonInfos.length) {
			let lastPersonInfo = newPersonInfos[newPersonInfos.length - 1];
			newPersonInfo['id'] = lastPersonInfo.id + 1;
		} else {
			newPersonInfo['id'] = 1;
		}

		setPersonInfo([...newPersonInfos, newPersonInfo]);
	};

	const updatePersonInfo = (updatedPersonInfo) => {
		console.log(updatedPersonInfo);
		let newPersonInfos = [...personInfo];

		newPersonInfos.map((info) => {
			if (info.id === updatedPersonInfo.id) {
				info.username = updatedPersonInfo.username;
				info.email = updatedPersonInfo.email;
				info.jobs = updatedPersonInfo.jobs;
			}
			return info;
		});

		setPersonInfo(newPersonInfos);
	};

	const selectePersonInfo = (selectedInfo) => {
		setSeletedPersonInfo(selectedInfo);
	};

	return (
		<div>
			<NavMenu />
			<PersonInfoCreate
				personInfo={selectedPersonInfo}
				addPersonInfo={addPersonInfo}
				updatePersonInfo={updatePersonInfo}
			/>
			<TableData data={personInfo} deletePersonInfo={deletePersonInfo} selectePersonInfo={selectePersonInfo} />
			<Page />
		</div>
	);
}

export default App;
