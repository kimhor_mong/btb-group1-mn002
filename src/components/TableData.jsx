import React from 'react';
import { Table, Button, ButtonGroup, Container } from 'react-bootstrap';

export default function TableData({ data, deletePersonInfo, selectePersonInfo }) {
	return (
		<Container>
			<h4>Display Data As: </h4>
			<ButtonGroup>
				<Button variant='secondary' active={true}>
					Table
				</Button>
				<Button variant='secondary'>Card</Button>
			</ButtonGroup>
			<Table bordered hover className='mt-3'>
				<thead>
					<tr>
						<th>#</th>
						<th>Name</th>
						<th>Gender</th>
						<th>Email</th>
						<th>Job</th>
						<th>Create At</th>
						<th>Updated At</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					{data.map((personInfo) => (
						<tr key={personInfo.id}>
							<td>{personInfo.id}</td>
							<td>{personInfo.username}</td>
							<td>{personInfo.gender}</td>
							<td>{personInfo.email}</td>
							<td>
								<ul>
									{personInfo.jobs.map((job) =>
										job.isChecked ? <li key={job.id}>{job.value}</li> : ''
									)}
								</ul>
							</td>
							<td>{personInfo.createdAt}</td>
							<td>{personInfo.updatedAt}</td>

							<td>
								<Button variant='primary' size='sm' className='mb-1'>
									<i className='fas fa-eye'> </i>
									<span className='ps-1'>View</span>
								</Button>

								<Button
									variant='info'
									size='sm'
									className='text-white mx-1 mb-1'
									onClick={() => selectePersonInfo(personInfo)}
								>
									<i className='fas fa-edit'></i>
									<span className='ps-1'>Update</span>
								</Button>

								<Button
									variant='danger'
									size='sm'
									className='mb-1'
									onClick={() => deletePersonInfo(personInfo.id)}
								>
									<i className='fas fa-trash-alt'></i>
									<span className='ps-1'>Delete</span>
								</Button>
							</td>
						</tr>
					))}
				</tbody>
			</Table>
		</Container>
	);
}
