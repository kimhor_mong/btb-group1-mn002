import React from 'react';
import { Container, Pagination } from 'react-bootstrap';

export default function Page() {
	return (
		<Container className='d-flex justify-content-center'>
			<Pagination>
				<Pagination.First />
				<Pagination.Item>{1}</Pagination.Item>
				<Pagination.Item>{2}</Pagination.Item>
				<Pagination.Item>{3}</Pagination.Item>
				<Pagination.Last />
			</Pagination>
		</Container>
	);
}
