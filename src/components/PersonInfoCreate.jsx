import React, { useEffect, useState } from 'react';
import { Row, Col, Form, Button, Container } from 'react-bootstrap';

var isUpdateOperation = false;

function PersonInfoCreate({ personInfo, addPersonInfo, updatePersonInfo }) {
	useEffect(() => {
		if ('username' in personInfo) {
			isUpdateOperation = true;
			setUsername(personInfo.username);
			setEmail(personInfo.email);
			setGender(personInfo.gender);
			setJob(personInfo.jobs);
		}
	}, [personInfo]);

	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [gender, setGender] = useState('Male');
	const [jobs, setJob] = useState([
		{ id: 1, value: 'Student', isChecked: true },
		{ id: 2, value: 'Teacher', isChecked: false },
		{ id: 3, value: 'Developer', isChecked: false },
	]);
	const handleUserInput = (e) => {
		e.preventDefault();
		if (isUpdateOperation) {
			let updatedInfo = {
				id: personInfo.id,
				username: username,
				email: email,
				gender: gender,
				createdAt: 'one minute ago',
				updatedAt: 'two minute ago',
				jobs: jobs,
			};

			updatePersonInfo(updatedInfo);
			isUpdateOperation = false;
		} else {
			let userInfo = {
				id: 0,
				username: username,
				email: email,
				gender: gender,
				createdAt: 'one minute ago',
				updatedAt: 'two minute ago',
				jobs: jobs,
			};

			addPersonInfo(userInfo);
		}

		setUsername('');
		setEmail('');
		setGender('Male');
		setJob([
			{ id: 1, value: 'Student', isChecked: true },
			{ id: 2, value: 'Teacher', isChecked: false },
			{ id: 3, value: 'Developer', isChecked: false },
		]);
	};

	return (
		<Container>
			<h1 className='mt-3'>Person Info</h1>
			<Form>
				<Row>
					<Col md={8}>
						<Form.Group className='my-3'>
							<Form.Label>Username:</Form.Label>
							<Form.Control
								type='username'
								placeholder='username'
								value={username}
								onChange={(e) => setUsername(e.target.value)}
							/>
						</Form.Group>
						<Form.Group className='mb-3'>
							<Form.Label>Email:</Form.Label>
							<Form.Control
								type='email'
								placeholder='email'
								value={email}
								onChange={(e) => setEmail(e.target.value)}
							/>
						</Form.Group>
					</Col>
					<Col md={4}>
						<Form.Group className='my-3'>
							<Form.Label className='d-block h4'>Gender</Form.Label>
							<Form.Check
								inline
								type='radio'
								label='Male'
								name='gender'
								value='Male'
								checked={gender === 'Male'}
								onChange={(e) => setGender(e.target.value)}
							/>
							<Form.Check
								inline
								type='radio'
								label='Female'
								name='gender'
								value='Female'
								checked={gender === 'Female'}
								onChange={(e) => setGender(e.target.value)}
							/>
						</Form.Group>

						<Form.Group>
							<Form.Label className='d-block h4'>Job</Form.Label>
							<Form.Check
								inline
								type='checkbox'
								label='Student'
								value='Student'
								name='job'
								checked={jobs[0]['isChecked']}
								onChange={() => {
									let newJob = [...jobs];
									newJob[0]['isChecked'] = !newJob[0]['isChecked'];
									setJob(newJob);
								}}
							/>
							<Form.Check
								inline
								type='checkbox'
								label='Teacher'
								value='Teacher'
								name='job'
								checked={jobs[1]['isChecked']}
								onChange={() => {
									let newJob = [...jobs];
									newJob[1]['isChecked'] = !newJob[1]['isChecked'];
									setJob(newJob);
								}}
							/>
							<Form.Check
								inline
								type='checkbox'
								label='Developer'
								value='Developer'
								name='job'
								checked={jobs[2]['isChecked']}
								onChange={() => {
									let newJob = [...jobs];
									newJob[2]['isChecked'] = !newJob[2]['isChecked'];
									setJob(newJob);
								}}
							/>
						</Form.Group>
					</Col>
				</Row>
				<Row>
					<Col className='text-end' md={8}>
						<Button variant='primary' className='me-2' onClick={handleUserInput}>
							Submit
						</Button>
						<Button variant='dark'>Cancel</Button>
					</Col>
				</Row>
			</Form>
		</Container>
	);
}

export default PersonInfoCreate;
